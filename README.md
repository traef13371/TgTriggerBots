[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/BergiuTelegram/TgTriggerBots)

**This is a private fork with an additional bot named Alpaka**
Its not my code and all the credits belong to [https://gitlab.com/BergiuTelegram/TgTriggerBots](https://gitlab.com/BergiuTelegram/TgTriggerBots)

# TelegramTriggerBots
This is a collection of telegram trigger bots and also the new repo for current updates on these bots.

## Bots
- [TelegramEntenBot](https://gitlab.com/BergiuTelegram/TgEntenBot)
- [TelegramHicksBot](https://gitlab.com/BergiuTelegram/TgHicksBot)
- [TelegramRMSBot](https://gitlab.com/BergiuTelegram/TgRMSBot)
- [TelegramWindoofBot](https://gitlab.com/BergiuTelegram/TgWindoofBot)

## Configuration
- Create a new TelegramBot with the [Botfather](https://telegram.me/botfather) for each of the above bots, that you would like to run
- Disable privacy settings for the bot and enable the bot in groups
- Write your Botname and your Token into the file `config/config.yml` (or in the shared volume if you use the docker installation) for each bot and enable them
	- **Don't use the same username and token for different bots!**

### Botfather Commands
This commands should be added to each bot in the [Botfather](https://telegram.me/botfather):
<code>
license - License
dsgvo - Privacy statement
</code>

## Docker

### Official forked build:
- [gitlab.com/traef13371/TgTriggerBots/container_registry](https://gitlab.com/traef13371/TgTriggerBots/container_registry)

### Official original build:
- [gitlab.com/BergiuTelegram/TgTriggerBots/container_registry](https://gitlab.com/BergiuTelegram/TgTriggerBots/container_registry)
### Release:

To build and push a new release to the gitlab docker registry, execute the `release.sh` script. If you havn't logged in already you need to create a new access token in gitlab and use it as passwort in the script.

### Forked Installation:
1. Run the container a first time with a shared volume
	- `docker run --name tgtriggerbots -v $HOSTFOLDER:/bot/config -d registry.gitlab.com/traef13371/TgTriggerBots:latest`
	- don't forget to replace or set the $HOSTFOLDER
	- or use the one i use:
	- `docker run --name tgtriggerbots -v tgtriggerbots:/bot/config -d registry.gitlab.com/traef13371/tgtriggerbots:lates`
2. Configure the bot-config in the shared volume
3. Start the container again
	- `docker start tgtriggerbots`

### Original Installation:
1. Run the container a first time with a shared volume
	- `docker run --name tgtriggerbots -v $HOSTFOLDER:/bot/config -d registry.gitlab.com/bergiutelegram/tgtriggerbots:latest`
	- don't forget to replace or set the $HOSTFOLDER
2. Configure the bot-config in the shared volume
3. Start the container again
	- `docker start tgtriggerbots`

## Without docker
### Dependencies
```
sudo apt install openjdk-8-jdk
```

### Forked Installation
```shell
git clone https://gitlab.com/traef13371/TgTriggerBots
./build
./run # to create the config folder and file-template
```

### Original Installation
```shell
git clone https://gitlab.com/BergiuTelegram/TgTriggerBots
./build
./run # to create the config folder and file-template
```

### Run
- `./run.sh`

### Development
- `source rc.sh`
