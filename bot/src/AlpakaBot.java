import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.Map;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

import java.sql.Timestamp;
import java.util.HashMap;

public class AlpakaBot extends TelegramLongPollingBot {
	/**
	 * A chat is blocked till a date and during this time
	 * the chat will not answer to any normal triggers
	 **/
	private class Block {
		private Timestamp till;

		/**
		 * Creates a block
		 **/
		 
		public Block(int minutes) {
			this.till = new Timestamp(System.currentTimeMillis()+(60000*minutes));
		}

		public boolean isBlocked() {
			Timestamp current = new Timestamp(System.currentTimeMillis());
			if (current.before(this.till)) {
				return true;
			}
			return false;
		}
	}

	private String botUsername;
	private String botToken;
	private String git;
	private String gitfork;
	private Map<Long, Block> blocks;
	private String dsgvo;

	public AlpakaBot(String username, String token, String git, String gitfork, String dsgvo) {
		this.botUsername = username.toLowerCase();
		this.botToken = token;
		this.git = git;
		this.gitfork = gitfork;
		this.blocks = new HashMap<Long, Block>();
		this.dsgvo = dsgvo;
	}

	@Override
	public String getBotUsername() {
		return this.botUsername;
	}

	@Override
	public String getBotToken() {
		return this.botToken;
	}

	@Override
	public void onUpdateReceived(Update update) {
		if (update.hasMessage() && update.getMessage().hasText()) {
			String text = update.getMessage().getText().toLowerCase();
			SendMessage message = new SendMessage();
			long chat_id = update.getMessage().getChatId();
			message.setChatId(chat_id);
			Block block = this.blocks.get(chat_id);
			// trigger every time
			if(text.equals("/license") || text.equals("/license"+this.getBotUsername())) {
				String msg = "Welcome!\nThis bot is a program which is available under the MIT license at " + this.git + "\nWas forked from the this repository " + this.gitfork;
				message.setText(msg);
			} else if(text.equals("/dsgvo") || text.equals("/dsgvo"+this.getBotUsername())) {
				String msg = this.dsgvo;
				message.setText(msg);
			}
			// don't trigger if is feeded
			else if(block == null || !block.isBlocked()) {
				if(block != null && !block.isBlocked()) {
					blocks.remove(block);
				}
				if ( text.contains("alpaka") ){
					if( text.contains("spritz")  || text.contains("schuss") || text.contains("nadel") ){
					message.setText("Mmhhh Frühstück!!!");
					this.blocks.put(chat_id, new Block(10));
					} else if ( text.contains("?") ){
					message.setText("Jiiiiiiip!");
					} else if ( text.contains("!!!") ){
					message.setText("WAS MAN WAS!?");
					} else if ( text.contains("aufstehen") ||  text.contains("aufzustehen") ||  text.contains("aufwachen") ||  text.contains("aufzuwachen") || text.contains("steh auf" ) ){
					message.setText("Ich glaub du hast drogen genommen!\n Ich bin bereits wach und jetzt chill deine Aura BRUDIII !!!😡🤬🔥🔥🔥");
					} else {
					message.setText("Jiiiiiiip!?");
					}
				}
				else if(text.contains("machen") || text.contains("kiffen") || text.contains("weed") || text.contains("gras")  || text.contains("joint")){
					message.setText("Erstmal ein kiffen...");
				} else if (text.contains("bier") || text.contains("alkohol") || text.contains("sterni") || text.contains("wein") || text.contains("sekt") || text.contains("zigarette") || text.contains("rauchen")){
					message.setText("Lieber erstmal ein kiffen...");
				} else if ( text.contains("was geht") || text.contains("sport") || text.contains("training") || text.contains("wandern") || text.contains("Laufen") ){
					message.setText("Lass mal lieber erstmal ein kiffen...");
				} else if ( text.contains("potenzial") || text.contains("potenz") || text.contains("band") || text.contains("musik") || text.contains("konzert") ){
					message.setText("Alpaka Potenzial!!!");
				} else if ( text.contains("acid") ||  text.contains("blotter") || text.contains("lsd") || text.contains("säure")){
					message.setText("Lsd ins Grundwasser!!!");
				} else if ( text.contains("salat") || text.contains("essig")){
					message.setText("Nur eine kleine prise Acid im Salat!!!");
				} else if ( text.contains("koks") || text.contains("kokain") || text.contains("cocain")){
					message.setText("RICHTIG DICKE LINES! 😍");
				} else if ( text.contains("nazi") || text.contains("fascho") || text.contains("faschist") || text.contains("afd")){
					message.setText("Ich hasse Nazis!!!");
				} else if ( text.contains("brot")){
					message.setText("Seh ich aus wie ne Ente?");
				} else if ( text.contains("quack")){
					/*message.setText("halts maul ente!");*/
				} else if ( text.contains("schnee") || text.contains("drogen")){
					message.setText("Schnee im Sommer! 😍");
				} else if ( text.contains("bubatz") || text.contains("scholz")){
					message.setText("Scholz wann bubatz!?");
				} else if ( text.contains("foos")){
					message.setText("Das Alpaka tröpfelt ein bisschen acid auf die brotkrümel und füttert es der ente...");
				} else if ( text.contains("ukraine")){
					message.setText("Krieg ist Scheisse!");
				} else if ( text.contains("putin")){
					message.setText("Das Alpaka distanziert sich von Putins Krieg!");
				} else {
					return;
				}
			} else if (text.contains("alpaka") && block.isBlocked() ){
					if ( text.contains("aufstehen") ||  text.contains("aufzustehen") ||  text.contains("aufwachen") ||  text.contains("aufzuwachen") || text.contains("steh auf" ) ){
						message.setText("jaja nerv nicht!");
						block.till =  new Timestamp(System.currentTimeMillis());
					} else if ( text.contains("spritz")  || text.contains("schuss") || text.contains("nadel") ){		
						long timeLeft = (block.till.getTime() - System.currentTimeMillis());
						long second = (timeLeft / 1000) % 60;
						long minute = (timeLeft / (1000 * 60)) % 60;			
						message.setText("*DER👏GEWÜNSCHTE👏GESPRÄCHSPARTNER👏IST👏TEMPORARILY👏NOT👏AVAILABLE👏!👏!👏!👏*\nDas Alapaka hatte sich einen Schuss gesetzt und befindet sich noch " + minute + " minuten und " + second + " sekunden im Rausch...");
					}
			} else {
				return;
			}
			message.setParseMode("markdown");
			try {
				sendMessage(message); // Call method to send the message
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}
		}
	}

}
