public class BotConfig {

	public boolean activated;
	public String username;
	public String token;

	public BotConfig(boolean activated, String username, String token) {
		this.activated = activated;
		this.username = username;
		this.token = token;
	}

}

