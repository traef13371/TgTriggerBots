#FROM gitpod/workspace-full

FROM openjdk:11-slim as build
MAINTAINER Marco Schlicht (marcoschlicht@onlinehome.de)

# install dependencies
RUN apt-get update
RUN apt-get install -y build-essential

# copy files
ADD bot/ /bot

# configure and build
RUN mkdir -p /bot/bin
RUN cd /bot && javac -g -d bin -cp "bin:lib/telegrambots-2.4.4.5-jar-with-dependencies.jar:lib/snakeyaml-1.19.jar" src/*.java

FROM openjdk:11-jre-slim
COPY --from=build /bot/ /bot/

# initial startup
VOLUME ["/bot/config"]

WORKDIR /bot

CMD ["/bot/run"]